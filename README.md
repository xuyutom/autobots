<div align="center"><a name="readme-top"></a>

<img src="./resources/logo.png" width="120" height="120" alt="autobots logo">
<h1>autobots</h1>

一个Agent+RPA开发平台。

## ⌨️ 本地开发

本项目分为前端和后端两个部分，前端项目在 app  目录下，后端项目在 server 目录下。这意味着，如果要运行 autobots，你就得同时启动前端和后端。项目启动后会在～ 目录创建 sqlite 数据库 autobots.db ，如果想查看数据库内容，建议使用开源数据库软件dbeaver。

### 启动前端

1. 安装 nodejs，要求nodejs版本为 v18.x；
2. 使用命令行进入到 app 目录；
3. 输入 npm install 安装依赖；
4. 输入 npm run dev 启动前端。

### 启动后端：

1. 安装python3，最好 3.9+版本。
2. 使用命令行进入 server 目录；
3. 创建并激活虚拟环境，输入 python -m venv .venv；
4. 输入 pip install -r requirements.txt 安装依赖；
5. 输入 flask --app main run 启动后端。

### 如果需要允许本地大模型：
1. 安装Ollma；
2. run code-qwen等代码大模型。